CFLAGS=-g -Werror -Wall -Wextra -pedantic -std=c11 -D_XOPEN_SOURCE=700
CXXFLAGS=-g -Wall -Wextra -pedantic -std=c++17 -D_XOPEN_SOURCE=700
LIBS=-lfl -lreadline
INCLUDE=-I build/include
ALL_SRC=$(shell find src/ -type 'f')
TEST_INCLUDE=-I build/test
GTEST_CFLAGS=$(shell pkg-config --cflags gtest)
GTEST_LIBS=$(shell pkg-config --libs gtest)
TEST_LIBS=$(LIBS) $(GTEST_LIBS)

ASAN=-fsanitize=address -fsanitize-address-use-after-scope -fno-omit-frame-pointer
#ASAN=

mysh: build/.marker build/types.o build/main.o build/parser.o build/helpers.o build/executor.o build/arguments.o
	@# $(LIBS) placed before object files causes the linker to fail on school machines with multiple main function definition error
	gcc -o mysh $(CFLAGS) $(ASAN) build/parser.o build/types.o build/main.o build/helpers.o build/executor.o build/arguments.o $(LIBS)

mysh-test: build/.marker build/types.o build/parser.o build/helpers.o build/executor.o build/arguments.o build/test_main.o
	g++ -o mysh-test $(CXXFLAGS) $(ASAN) build/types.o build/parser.o build/helpers.o build/executor.o build/arguments.o build/test_main.o $(TEST_LIBS)

build/.marker: $(ALL_SRC)
	-rm -r build
	cp -a src build
	touch build/.marker

build/parser.c: build/.marker build/parser/parser.l
	flex -o build/parser.c build/parser/parser.l

clean:
	-rm -r build
	-rm mysh
	-rm mysh-test
	-rm -rf test

build/test_main.o: build/.marker
	g++ -c $(CXXFLAGS) $(INCLUDE) $(TEST_INCLUDE) $(GTEST_CFLAGS) -o build/test_main.o build/tests/test_main.cpp

%.o: %.c
	gcc -c $(CFLAGS) $(INCLUDE) $(ASAN) -o $@ $<

.PHONY: test
test: mysh mysh-test
	mkdir -p test
	if test -d test/labs; then \
		git -C test/labs pull; \
	else \
		git clone 'https://github.com/devnull-cz/unix-linux-prog-in-c-labs.git' test/labs; \
	fi

	if test -d test/stef; then \
		git -C test/stef pull; \
	else \
		git clone 'https://github.com/devnull-cz/stef.git' test/stef; \
	fi

	echo 'export MYSH="../../../../mysh"' > test/labs/final-assignment-tests/shell/test-config.local
	echo 'export STEF="../../../stef/stef.sh"' >> test/labs/final-assignment-tests/shell/test-config.local

	./mysh-test && cd test/labs/final-assignment-tests/shell/ && ./run-tests.sh

.PHONY: run
run: mysh
	@./mysh

.PHONY: format
format:
	@echo Be careful with formatting the parser. It usually produces only junk!
	clang-format -i --style=Google $(ALL_SRC)
