# mysh - simple UNIX shell

## Project build, test, etc

|  goal | command |
|-------|---------|
|build| `make`|
| test | `make test`|
| run | `make run` or `./mysh` |

## Notes

* All builds have ASAN compiled in. In my opinion, there is no point in not including it, because performance is irelevant in this case.
* Tests are written in C++, because Google Test is designed for C++. But pretty much whole project is in plain C.

