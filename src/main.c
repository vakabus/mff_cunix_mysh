#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <err.h>
#include <fcntl.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <signal.h>
#include <stdbool.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "arguments.h"
#include "executor.h"
#include "helpers.h"
#include "parser.h"
#include "types.h"

static bool stuck_in_readline = false;
static bool readline_command_read = false;
static char *read_buffer = NULL;

static size_t get_file_size(int32_t fd) {
  off_t off = lseek(fd, 0, SEEK_END);
  if (off == -1) err(1, "error detecting size using seek");

  off_t p = lseek(fd, 0, SEEK_SET);
  if (p == -1) err(1, "lseek");

  return (size_t)off;
}

static char *read_whole_file(char *filename) {
  int fd = open(filename, O_RDONLY);
  if (fd == -1) err(1, "open");

  size_t size = get_file_size(fd);
  char *buff = safe_malloc(size + 1);
  buff[size] = 0;  // null byte at the end
  size_t r = read(fd, buff, size);
  if (r == (size_t)-1) err(1, "read");
  assert(r == size);
  close(fd);

  return buff;
}

static char *myreadline(args_t *args) {
  // read from argument
  if (args->command != NULL) {
    if (readline_command_read) {
      return NULL;
    } else {
      readline_command_read = true;
      return args->command;
    }
  }

  // read from file
  if (args->infile != NULL) {
    if (read_buffer == NULL) {
      read_buffer = read_whole_file(args->infile);
      assert(read_buffer != NULL);
      return read_buffer;
    } else {
      return NULL;
    }
  }

  // read interactive
  stuck_in_readline = true;
  char *res = readline("mysh$ ");
  stuck_in_readline = false;
  add_history(res);
  return res;
}

static void sigint_handler(int UNUSED(signo)) {
  if (stuck_in_readline) {
    write(STDOUT_FILENO, "\n", 1);  // Move to a new line
    rl_on_new_line();               // Regenerate the prompt on a newline
    rl_replace_line("", 0);         // Clear the previous text
    rl_redisplay();
  }

  executor_send_sigint();
}

static void handle_input_line(char *line) {
  assert(line != NULL);
  parser_set_input_string(line);

  while (true) {
    command_t *cmd = parser_next();

    // input end wait
    if (cmd == NULL) {
      executor_wait();
      break;
    }

    // command_debug_print(cmd);
    executor_execute(cmd);

    // pipeline end wait
    if (cmd->pipe_output == false) {
      executor_wait();
    }

    command_free(cmd);
  }

  free(line);
}

int main(int argc, char **argv) {
  args_t *args = args_parse(argc, argv);

  // signal handlers
  if (is_interactive(args)) {
    struct sigaction psa;
    psa.sa_handler = sigint_handler;
    psa.sa_flags = SA_RESTART;
    sigemptyset(&psa.sa_mask);
    sigaction(SIGINT, &psa, NULL);
  }

  while (true) {
    char *line = myreadline(args);
    if (line == NULL) break;
    handle_input_line(line);
  }

  if (is_interactive(args)) printf("\n");

  free(args);

  return executor_destroy();
}
