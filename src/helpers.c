#include "helpers.h"

#include <err.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

char *bool_to_string(bool val) { return (val ? "true" : "false"); }

void *safe_malloc(size_t size) {
  void *ptr = malloc(size);
  if (ptr == NULL) err(1, "malloc");

  return ptr;
}

char *make_text_copy(char *text) {
  int len = strlen(text) + 1;
  char *ntext = safe_malloc(len);
  strncpy(ntext, text, len);
  return ntext;
}
