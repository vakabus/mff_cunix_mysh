#ifndef _HELPERS_H
#define _HELPERS_H

#include <stdbool.h>
#include <stddef.h>

char* bool_to_string(bool val);
char* make_text_copy(char* text);
void* safe_malloc(size_t size);

/* Source: https://stackoverflow.com/questions/7090998/portable-unused-parameter-macro-used-on-function-signature-for-c-and-c */
#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
# define UNUSED(x) /*@unused@*/ x
#elif defined(__cplusplus)
# define UNUSED(x)
#else
# define UNUSED(x) x
#endif

#endif
