#ifndef _ARGUMENTS_H
#define _ARGUMENTS_H

#include <stdbool.h>

typedef struct args {
  char* infile;
  char* command;
} args_t;

args_t* args_parse(int argc, char** argv);
bool is_interactive(args_t* args);

#endif
