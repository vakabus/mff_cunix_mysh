#ifndef _TYPES_H
#define _TYPES_H

#include <stdbool.h>
#include <sys/queue.h>

typedef enum command_type { EXTERNAL, CD, EXIT } command_type_t;

typedef struct command_cd {
  char* directory;
} command_cd_t;

typedef struct command_external_arg {
  char* arg;
  TAILQ_ENTRY(command_external_arg) next;
} command_external_arg_t;

typedef TAILQ_HEAD(command_argument_list,
                   command_external_arg) command_arg_list_head_t;

typedef struct command_external {
  command_arg_list_head_t arguments;
} command_external_t;

typedef struct command_exit {
  // nothing
  char dummy;
} command_exit_t;

typedef struct command {
  command_type_t type;
  char* redir_stdin;
  char* redir_stdout;
  bool redir_stdout_append;
  bool pipe_output;
  union {
    command_cd_t cd;
    command_external_t external;
    command_exit_t exit;
  } data;
} command_t;

void command_debug_print(command_t* cmd);
char* command_type_to_string(command_type_t type);
void command_free(command_t* cmd);
char** command_external_to_arg_array(command_t* cmd);
void command_rewrite_as_true(command_t* cmd);

#endif
