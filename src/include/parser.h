#ifndef _PARSER_H
#define _PARSER_H

#include "types.h"

void parser_set_input_string(char* input);
command_t* parser_next();

#endif
