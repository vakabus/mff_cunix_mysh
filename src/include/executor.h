#ifndef _EXECUTOR_H
#define _EXECUTOR_H

#include <stdint.h>

#include "types.h"

void executor_execute(command_t* cmd);
void executor_wait(void);
uint32_t executor_destroy(void);
void executor_send_sigint(void);

#endif
