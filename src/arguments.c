#include "arguments.h"

#include <err.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "helpers.h"

extern char *optarg;
extern int optind, opterr, optopt;

args_t *args_parse(int argc, char **argv) {
  // reset the getopt parser
  optind = 1;

  args_t *args = safe_malloc(sizeof(args_t));
  args->command = NULL;
  args->infile = NULL;

  int optres;
  while ((optres = getopt(argc, argv, "c:")) != -1) {
    switch (optres) {
      case '?': {
        errx(1, "Unknown option %s", argv[optind - 1]);
      }
      case 'c': {
        int len = strlen(optarg);
        args->command = safe_malloc(len + 1);
        args->command[len] = 0;
        strncpy(args->command, optarg, len);
        break;
      }
    }
  }

  if (optind < argc) {
    args->infile = argv[optind];
  }

  if (args->infile != NULL && args->command != NULL)
    errx(1, "invalid arguments, input file and command are exclusive options");

  return args;
}

bool is_interactive(args_t *args) {
  return isatty(STDIN_FILENO) && args->command == NULL && args->infile == NULL;
}
