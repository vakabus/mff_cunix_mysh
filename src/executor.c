#include "executor.h"

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <pwd.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "helpers.h"
#include "types.h"

static int stdin_fd = STDIN_FILENO;
static int stdout_fd = STDOUT_FILENO;
static int stderr_fd = STDERR_FILENO;

static const int stdin_fd_backup = 253;
static const int stdout_fd_backup = 254;
static const int stderr_fd_backup = 255;

static bool executor_initialized = false;

static pid_t last_executed_pid = 0;
static uint8_t last_exit_code = 0;

static int pipefd[2];

static bool stuck_in_wait = false;

static struct running {
  uintptr_t size;
  pid_t *pids;
} running = {.size = 0, .pids = NULL};

static void add_running_pid(pid_t pid) {
  running.pids = realloc(running.pids, sizeof(pid_t) * (running.size + 1));
  running.pids[running.size] = pid;
  running.size++;
}

static void remove_running_pid(pid_t pid) {
  pid_t *narr = safe_malloc(sizeof(pid_t) * (running.size - 1));
  uintptr_t j = 0;
  for (uintptr_t i = 0; i < running.size; i++) {
    if (running.pids[i] != pid) {
      narr[j] = running.pids[i];
      j++;
    }
  }

  free(running.pids);
  running.pids = narr;
  running.size = j;
}

static void executor_init() {
  dup2(stdin_fd, stdin_fd_backup);
  dup2(stdout_fd, stdout_fd_backup);
  dup2(stderr_fd, stderr_fd_backup);

  executor_initialized = true;
}

static void restore_fd_backup() {
  dup2(stdin_fd_backup, stdin_fd);
  dup2(stdout_fd_backup, stdout_fd);
  dup2(stderr_fd_backup, stderr_fd);
}

static void close_fd_backup() {
  close(stdin_fd_backup);
  close(stdout_fd_backup);
  close(stderr_fd_backup);
}

static void executor_chdir(command_t *cmd) {
  assert(cmd->type == CD);
  if (cmd->data.cd.directory == NULL) {
    // cd $HOME
    struct passwd *pw = getpwuid(getuid());
    cmd->data.cd.directory = make_text_copy(pw->pw_dir);
  } else if (strcmp(cmd->data.cd.directory, "-") == 0) {
    // cd $OLDPWD
    assert(getenv("OLDPWD") != NULL);
    free(cmd->data.cd.directory);
    cmd->data.cd.directory = make_text_copy(getenv("OLDPWD"));
    printf("%s\n",
           cmd->data.cd
               .directory);  // FIXME is this correct? should we output it here?
  }

  char buff[1024];  // should be enough, if not, it will fail in the next step

  if (NULL == getcwd(buff, 1024)) err(1, "getcwd");
  setenv("OLDPWD", buff, true);

  int res = chdir(cmd->data.cd.directory);
  if (res == -1)
    fprintf(stderr, "mysh: cd: %s: %s\n", cmd->data.cd.directory,
            strerror(errno));
  free(
      cmd->data.cd.directory);  // because we will do a rewrite of the structure

  if (NULL == getcwd(buff, 1024)) err(1, "getcwd");
  setenv("PWD", buff, true);
}

static void executor_exit(command_t *cmd) {
  (void)cmd;

  uint32_t exit_code = executor_destroy();
  exit(exit_code);
}

static void executor_execute_child(command_t *cmd) {
  close_fd_backup();

  assert(cmd->type == EXTERNAL);  // we running inside a fork. Internal commands
                                  // have no meaning here

  if (cmd->redir_stdin != NULL) {
    close(0);
    int o = open(cmd->redir_stdin, O_RDONLY);
    if (o == -1) err(1, "open");
    assert(o == 0);
  }

  if (cmd->redir_stdout != NULL) {
    close(1);
    int o = open(cmd->redir_stdout,
                 O_WRONLY | O_CREAT | (cmd->redir_stdout_append ? O_APPEND : 0),
                 0666);
    if (o == -1) err(1, "open");
    assert(o == 1);
  }

  char **args = command_external_to_arg_array(cmd);
  execvp(args[0], args);
  if (errno == ENOENT) {
    fprintf(stderr, "%s\n", strerror(errno));
    exit(127);  // unknown command exit code
  }
  err(1, "exec");
}

static void executor_execute_parent(command_t *cmd) {
  if (cmd->pipe_output) {
    dup2(stdout_fd_backup,
         stdout_fd);  // restore stdout from backup (closes the pipe)
    dup2(pipefd[0], stdin_fd);
    close(pipefd[0]);
  } else {
    restore_fd_backup();
  }
}

void executor_execute(command_t *cmd) {
  if (!executor_initialized) executor_init();

  // special handling for internal commands
  if (cmd->type == CD) {
    executor_chdir(cmd);
  } else if (cmd->type == EXIT) {
    executor_exit(cmd);
  }
  if (cmd->type != EXTERNAL) {
    // internal commands are rewritten as true command to handle them inside
    // pipelines
    command_rewrite_as_true(cmd);
  }

  // pipelines
  if (cmd->pipe_output) {
    pipe(pipefd);
    dup2(pipefd[1], stdout_fd);
    close(pipefd[1]);
  }

  pid_t pid = fork();
  switch (pid) {
    case -1:
      err(1, "fork");
    case 0: {
      executor_execute_child(cmd);
      break;
    }
    default: {
      add_running_pid(pid);
      last_executed_pid = pid;
      executor_execute_parent(cmd);
      break;
    }
  }
}

void executor_wait() {
  stuck_in_wait = true;
  while (true) {
    int status;

    pid_t pid = wait(&status);

    if (pid == -1 && errno == ECHILD) break;
    if (pid == -1) err(1, "wait");

    if (WIFSIGNALED(status)) {
      if (pid == last_executed_pid) last_exit_code = 128 + WTERMSIG(status);
      remove_running_pid(pid);
      printf("Killed by signal %d\n", WTERMSIG(status));
    } else if (WIFEXITED(status)) {
      if (pid == last_executed_pid) last_exit_code = WEXITSTATUS(status);
      remove_running_pid(pid);
    } else {
      printf("something happened with the child, but we are not sure what\n");
    }
  }

  stuck_in_wait = false;
  assert(running.size == 0);
}

uint32_t executor_destroy() {
  executor_wait();  // wait for everything. Nothing should be running, but this
                    // won't hurt anything
  return last_exit_code;
}

void executor_send_sigint() {
  if (!stuck_in_wait) return;

  for (uintptr_t i = 0; i < running.size; i++) {
    if (-1 == kill(running.pids[i], SIGINT)) err(1, "kill");
  }
}
