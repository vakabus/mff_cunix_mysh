#include <gtest/gtest.h>

#include "test_arguments.h"
#include "test_parser.h"

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
