#pragma once

#include <gtest/gtest.h>

#include "helper.h"

extern "C" {
#include "parser.h"
#include "types.h"
}

class Parser : public ::testing::Test {
 protected:
  virtual void TearDown() override { collect_garbage(); }
};

TEST_F(Parser, SimpleCommand) {
  parser_set_input_string(clone("  some\t command    "));
  command_t* cmd = parser_next();

  // the result should be a command
  ASSERT_EQ(cmd->type, EXTERNAL);

  // check values of the arguments
  char** args = command_external_to_arg_array(cmd);
  ASSERT_STREQ(args[0], clone("some"));
  ASSERT_STREQ(args[1], clone("command"));
  ASSERT_EQ(args[2], nullptr);

  // there should be nothing more given by the parser
  ASSERT_EQ(parser_next(), nullptr);

  // cleanup
  command_free(cmd);
  free(args);
}

TEST_F(Parser, Cd) {
  parser_set_input_string(clone("\tcd cd"));
  command_t* cmd = parser_next();

  // the result should be a internal cd
  ASSERT_NE(cmd, nullptr);
  ASSERT_EQ(cmd->type, CD);
  ASSERT_STREQ(cmd->data.cd.directory, clone("cd"));

  // there is nothing more
  ASSERT_EQ(parser_next(), nullptr);

  command_free(cmd);
}

TEST_F(Parser, CdEmpty) {
  parser_set_input_string(clone("\tcd\t"));
  command_t* cmd = parser_next();

  // the result should be a internal cd
  ASSERT_NE(cmd, nullptr);
  ASSERT_EQ(cmd->type, CD);
  ASSERT_STREQ(cmd->data.cd.directory, nullptr);

  // there is nothing more
  ASSERT_EQ(parser_next(), nullptr);

  command_free(cmd);
}

/*
// Works, but invalidas the state of the parser for future use
TEST(Parser, CdDoubleInvalid) {
    parser_set_input_string(clone("\tcd somewhere something\t"));

    ASSERT_DEATH({
        parser_next();
    }, clone(".*"));
}
*/

TEST_F(Parser, PipeSimple) {
  parser_set_input_string(clone("command1 | command2 arg1 | command3 arg3"));

  // first command
  command_t* cmd = parser_next();
  ASSERT_NE(cmd, nullptr);
  ASSERT_EQ(cmd->type, EXTERNAL);
  ASSERT_TRUE(cmd->pipe_output);
  command_free(cmd);

  // second command
  cmd = parser_next();
  ASSERT_NE(cmd, nullptr);
  ASSERT_EQ(cmd->type, EXTERNAL);
  ASSERT_TRUE(cmd->pipe_output);
  command_free(cmd);

  // third command
  cmd = parser_next();
  ASSERT_NE(cmd, nullptr);
  ASSERT_EQ(cmd->type, EXTERNAL);
  ASSERT_FALSE(cmd->pipe_output);
  command_free(cmd);

  // there is nothimg more
  cmd = parser_next();
  ASSERT_EQ(cmd, nullptr);
}

TEST_F(Parser, Redirection) {
  parser_set_input_string(clone("> gghh cmd < input > output"));

  // first command
  command_t* cmd = parser_next();
  ASSERT_NE(cmd, nullptr);
  ASSERT_EQ(cmd->type, EXTERNAL);
  ASSERT_FALSE(cmd->pipe_output);
  ASSERT_STREQ(cmd->redir_stdin, clone("input"));
  ASSERT_STREQ(cmd->redir_stdout, clone("output"));
  ASSERT_FALSE(cmd->redir_stdout_append);
  char** args = command_external_to_arg_array(cmd);
  ASSERT_STREQ(args[0], clone("cmd"));
  ASSERT_EQ(args[1], nullptr);

  ASSERT_EQ(parser_next(), nullptr);

  command_free(cmd);
  free(args);
}

TEST_F(Parser, Exit) {
  parser_set_input_string(clone("exit"));
  command_t* cmd = parser_next();

  // the result should be a internal cd
  ASSERT_NE(cmd, nullptr);
  ASSERT_EQ(cmd->type, EXIT);
  command_free(cmd);

  // there is nothing more
  ASSERT_EQ(parser_next(), nullptr);
}
