#pragma once

#include <stdlib.h>
#include <string.h>

std::vector<char*> strings;

/**
 * This is a wrapper for string literals which converts them from const char* to
 *char* by copying them to newly allocated space. To clean up, call the garbage
 *collector function.
 **/
char* clone(const char* str) {
    char* data = (char*)malloc(strlen(str) + 1);
    strings.push_back(data);
    strcpy(data, str);
    return data;
}

void collect_garbage() {
  for (char* ptr : strings) free(ptr);
  strings.clear();
}
