#pragma once

#include <gtest/gtest.h>

extern "C" {
#include "arguments.h"
#include "types.h"
}

#include "helper.h"

class Arguments : public ::testing::Test {
 protected:
  virtual void TearDown() override { collect_garbage(); }
};

TEST_F(Arguments, SingleCommand) {
  char* argv[3];
  argv[0] = clone("mysh");
  argv[1] = clone("-c");
  argv[2] = clone("cmd");

  args_t* args = args_parse(3, argv);

  ASSERT_EQ(args->infile, nullptr);
  ASSERT_STREQ(args->command, argv[2]);

  if (args->command != NULL) free(args->command);
  free(args);
}

TEST_F(Arguments, FileInput) {
  char* argv[2];
  argv[0] = clone("mysh");
  argv[1] = clone("file");

  args_t* args = args_parse(2, argv);

  ASSERT_EQ(args->command, nullptr);
  ASSERT_EQ(args->infile,
            argv[1]);  // the pointer equivalence is here on purpose

  if (args->command != NULL) free(args->command);
  free(args);
}

TEST_F(Arguments, BothFileAndCommand) {
  char* argv[4];
  argv[0] = clone("mysh");
  argv[1] = clone("-c");
  argv[2] = clone("cmd");
  argv[3] = clone("infile");

  // this combination of Arguments is not allowed
  args_t* args = NULL;
  ASSERT_DEATH({ args = args_parse(4, argv); }, ".*");

  if (args != NULL && args->command != NULL) free(args->command);
  if (args != NULL) free(args);
}
