%{
/* this will be at the top of the source file */
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <err.h>

#include "types.h"
#include "parser.h"
#include "helpers.h"

#define YY_DECL command_t* yylex()

static command_external_arg_t* create_arg(char* text) {
    command_external_arg_t* arg = safe_malloc(sizeof(command_external_arg_t));
    arg->arg = make_text_copy(text);
    return arg;
}

uint32_t line_number = 0;

%}

%option never-interactive
%option warn nodefault
%option noyywrap noinput nounput

WHITESPACE [ \t]
NOT_WHITESPACE [^ \n\t]
COMMAND_END [\n\0;|]
NON_SPECIAL [^ \t\n;|\0#<>]

%x CD
%x CD_END
%x COMMAND
%x INITIAL_PIPE
%x COMMENT
%x REDIR_IN
%x REDIR_OUT

%%

%{
    // this code will be at the beggining of the parser function
    char* cd_dir = NULL;
    command_t* command = calloc(sizeof(command_t), 1);  // value from argument
    if (command == NULL) err(1, "calloc");
    command->pipe_output = false;
%}

<CD>[ \t]+                              /* ignore whitespace */
<CD>{NON_SPECIAL}+                      {   if (cd_dir != NULL) errx(1, "Parsing error");
                                            else cd_dir = make_text_copy(yytext);
                                            BEGIN(CD_END);
                                        }
<CD>{COMMAND_END}                       {   return command;
                                        }
<CD><<EOF>>                             {   BEGIN(INITIAL);
                                            return command;
                                        }
<CD_END>{COMMAND_END}                   {   command->data.cd.directory = cd_dir;
                                            BEGIN(INITIAL);
                                            return command;
                                        }
<CD_END><<EOF>>                         {   command->data.cd.directory = cd_dir;
                                            BEGIN(INITIAL);
                                            return command;
                                        }
<CD_END>[\t ]                           /* ignore whitespace */
<CD_END>.                               {   errx(1, "Invalid cd syntax - too many arguments");
                                            BEGIN(INITIAL);
                                        }

 /* redirections */
<INITIAL,INITIAL_PIPE><[ \t]*           {   BEGIN(REDIR_IN);
                                            command->type = EXTERNAL;
                                            TAILQ_INIT(&(command->data.external.arguments));
                                        }
<INITIAL,INITIAL_PIPE>>>[ \t]*          {   BEGIN(REDIR_OUT);
                                            command->redir_stdout_append = true;
                                            command->type = EXTERNAL;
                                            TAILQ_INIT(&(command->data.external.arguments));
                                        }
<INITIAL,INITIAL_PIPE>>[ \t]*           {   BEGIN(REDIR_OUT);
                                            command->redir_stdout_append = false;
                                            command->type = EXTERNAL;
                                            TAILQ_INIT(&(command->data.external.arguments));
                                        }
<COMMAND>\<[ \t]*                       {   BEGIN(REDIR_IN); }
<COMMAND>\>\>[ \t]*                     {   BEGIN(REDIR_OUT);
                                            command->redir_stdout_append = true;
                                        }
<COMMAND>\>[ \t]*                       {   BEGIN(REDIR_OUT);
                                            command->redir_stdout_append = false;
                                        }
<REDIR_IN>{NON_SPECIAL}+                {   if (command->redir_stdin != NULL) free(command->redir_stdin);
                                            command->redir_stdin = make_text_copy(yytext);
                                            BEGIN(COMMAND);
                                        }
<REDIR_OUT>{NON_SPECIAL}+               {   if (command->redir_stdout != NULL) free(command->redir_stdout);
                                            command->redir_stdout = make_text_copy(yytext);
                                            BEGIN(COMMAND);
                                        }
<REDIR_OUT>.                            errx(1, "syntax error. unexpected '%s' on line %d", yytext, line_number);

 /* command */
<COMMAND>[\t ]+                         /* ignore whitespace */
<COMMAND>{NON_SPECIAL}+                 {   command_external_t* ptr = &(command->data.external);
                                            command_external_arg_t* arg = create_arg(yytext);
                                            TAILQ_INSERT_TAIL(&(ptr->arguments), arg, next);
                                        }
<COMMAND>\n                             {   line_number++;
                                            BEGIN(INITIAL);
                                            return command; }
<COMMAND>;                              {   BEGIN(INITIAL);
                                            return command;
                                        }
<COMMAND><<EOF>>                        {   BEGIN(INITIAL);
                                            return command;
                                        }
<COMMAND>\|                             {   command->pipe_output = true;
                                            BEGIN(INITIAL_PIPE);
                                            return command;
                                        }
<COMMAND>#                              {   BEGIN(COMMENT);
                                            return command;
                                        }
<COMMAND>.                              fprintf(stderr, "unmatched %s\n", yytext);

<COMMENT>[^\n]+                         /* ignore comment content */
<COMMENT>\n                             {   line_number++;
                                            BEGIN(INITIAL);
                                        }
<COMMENT><<EOF>>                        {   free(command);
                                            return NULL;
                                        }

<INITIAL,INITIAL_PIPE>{WHITESPACE}+     /* eat up whitespace */
<INITIAL,INITIAL_PIPE>[;|]              errx(1, "syntax error on line %d!", line_number);
<INITIAL,INITIAL_PIPE>exit              {   command->type = EXIT;
                                            return command;
                                        }
<INITIAL,INITIAL_PIPE>cd                {   BEGIN(CD);
                                            command->type = CD;
                                        }
<INITIAL,INITIAL_PIPE>{NON_SPECIAL}+    {   BEGIN(COMMAND);
                                            command->type = EXTERNAL;
                                            TAILQ_INIT(&(command->data.external.arguments));
                                            command_external_arg_t* arg = create_arg(yytext);
                                            TAILQ_INSERT_TAIL(&(command->data.external.arguments), arg, next);
                                        }
<INITIAL>#                              BEGIN(COMMENT);
<INITIAL_PIPE>#                         errx(1, "because multiline commands are not supported, pipe can not be followed by a command");
<INITIAL>\n                             line_number++;
<INITIAL_PIPE>\n                        errx(1, "non-terminated pipeline");
<INITIAL><<EOF>>                        {   free(command); return NULL;
                                        }
<INITIAL_PIPE><<EOF>>                   errx(1, "non-terminated pipeline");
<*>.                                    {   printf("unexpected char %s\n", yytext); errx(1, "something is wrong with parsing");
                                        }

%%


bool parser_finished = true;
YY_BUFFER_STATE buffer_handle = NULL;
void parser_set_input_string(char* input) {
    assert(parser_finished);

    buffer_handle = yy_scan_string(input);
    parser_finished = false;
}

command_t* parser_next() {
    assert(!parser_finished);

    command_t* arg = yylex();
    if (arg == NULL) {
        yy_delete_buffer(buffer_handle);
        parser_finished = true;
        yylex_destroy();
    };

    return arg;
}
