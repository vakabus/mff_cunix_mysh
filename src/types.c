#include "types.h"

#include <assert.h>
#include <err.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>

#include "helpers.h"

void command_debug_print(command_t *cmd) {
  fprintf(stderr, "|-------\n");
  fprintf(stderr, "| Command:\n");
  fprintf(stderr, "|-------\n");
  fprintf(stderr, "| Pipe:\t\t%s\n", bool_to_string(cmd->pipe_output));
  fprintf(stderr, "| Type:\t\t%s\n", command_type_to_string(cmd->type));
  fprintf(stderr, "| Redir out:\t\t%s\n", cmd->redir_stdout);
  fprintf(stderr, "| Redir out append:\t%s\n",
          bool_to_string(cmd->redir_stdout_append));
  fprintf(stderr, "| Redir in:\t\t%s\n", cmd->redir_stdin);
  switch (cmd->type) {
    case CD:
      fprintf(stderr, "| Directory:\t'%s'\n", cmd->data.cd.directory);
      break;
    case EXIT:
      /* do nothing */
      break;
    case EXTERNAL:
      fprintf(stderr, "| Arguments:\n");
      command_external_arg_t *arg = NULL;
      TAILQ_FOREACH(arg, &(cmd->data.external.arguments), next) {
        fprintf(stderr, "|    %s\n", arg->arg);
      }
      break;
    default:
      errx(1, "unexpected command type");
  }
  fprintf(stderr, "|-------\n");
}

char *command_type_to_string(command_type_t type) {
  switch (type) {
    case CD:
      return "CD";
    case EXTERNAL:
      return "EXTERNAL";
    case EXIT:
      return "EXIT";
    default:
      errx(1, "unexpected command type");
  }
}

void command_free(command_t *cmd) {
  if (cmd->redir_stdin != NULL) free(cmd->redir_stdin);
  if (cmd->redir_stdout != NULL) free(cmd->redir_stdout);

  if (cmd->type == EXTERNAL) {
    command_external_arg_t *arg = NULL;
    command_external_arg_t *arg_prev = NULL;
    TAILQ_FOREACH(arg, &(cmd->data.external.arguments), next) {
      free(arg->arg);
      if (arg_prev != NULL) free(arg_prev);
      arg_prev = arg;
    }
    if (arg_prev != NULL) free(arg_prev);
  } else if (cmd->type == CD) {
    free(cmd->data.cd.directory);
  } else if (cmd->type == EXIT) {
    // nothing
  } else {
    assert(false);
  }
  free(cmd);
}

uintptr_t command_get_argument_count(command_t *cmd) {
  uint32_t count = 0;
  command_external_arg_t *arg = NULL;
  TAILQ_FOREACH(arg, &(cmd->data.external.arguments), next) { count++; }
  return count;
}

char **command_external_to_arg_array(command_t *cmd) {
  assert(cmd->type == EXTERNAL);

  uintptr_t len = command_get_argument_count(cmd);
  assert(len >= 1);
  char **arr = safe_malloc((len + 1) * sizeof(char *));
  uintptr_t i = 0;
  command_external_arg_t *arg = NULL;
  TAILQ_FOREACH(arg, &(cmd->data.external.arguments), next) {
    arr[i] = arg->arg;
    i++;
  }
  arr[i] = NULL;

  assert(arr != NULL);
  return arr;
}

void command_rewrite_as_true(command_t *cmd) {
  cmd->type = EXTERNAL;
  command_arg_list_head_t *args = &(cmd->data.external.arguments);
  TAILQ_INIT(args);

  command_external_arg_t *arg = safe_malloc(sizeof(command_external_arg_t));
  arg->arg = safe_malloc(strlen("true") + 1);
  arg->arg[4] = 0;
  strcpy(arg->arg, "true");

  TAILQ_INSERT_TAIL(args, arg, next);
}
